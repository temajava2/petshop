package myClinic;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the appointment database table.
 * 
 */
@Entity
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idappointment;

	@Temporal(TemporalType.DATE)
	private Date date;

	private Time hour;

	private String type;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idanimal")
	private Animal animal;

	//bi-directional many-to-one association to Medicalstuff
	@ManyToOne
	@JoinColumn(name="idmedicalStuff")
	private Medicalstuff medicalstuff;

	public Appointment() {
	}

	public int getIdappointment() {
		return this.idappointment;
	}

	public void setIdappointment(int idappointment) {
		this.idappointment = idappointment;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getHour() {
		return this.hour;
	}

	public void setHour(Time hour) {
		this.hour = hour;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Medicalstuff getMedicalstuff() {
		return this.medicalstuff;
	}

	public void setMedicalstuff(Medicalstuff medicalstuff) {
		this.medicalstuff = medicalstuff;
	}

}