package controllers;

import java.net.URL;
import java.sql.Time;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.scene.control.Label;

//import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

//import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
//import javafx.scene.control.cell.PropertyValueFactory;
//import javafx.util.Callback;
//import myClinic.Animal;
import myClinic.Appointment;
import myClinic.SimplifiedAppointment;
//import myClinic.Medicalstuff;
import util.DatabaseUtil;

public class MainController implements Initializable {

	@FXML
	private TableView<SimplifiedAppointment> tableId;
	@FXML
	private Label nameLabel;
	private Label ageLabel;
	private Label typeLable;
	private Label raceLabel;
	private Label nameOwnerLabel;
	private Label phoneLable;
	
	
	
	public void populateTableFromDB() {
		tableId.setEditable(true);
		DatabaseUtil db = new DatabaseUtil();
		db.setConnectionToDatabase();
		db.start();
		
		List<Appointment> apps = (List<Appointment>) db.appointmentList();
		
		TableColumn<SimplifiedAppointment, String> visitType = new TableColumn<SimplifiedAppointment, String>("Visit");
		visitType.setCellValueFactory(new PropertyValueFactory<>("type"));
		
		TableColumn<SimplifiedAppointment, Time> hourColumn = new TableColumn<SimplifiedAppointment, Time>("Hour");
		hourColumn.setCellValueFactory(new PropertyValueFactory<>("hour"));
		
		ObservableList<SimplifiedAppointment> appLIST = FXCollections.observableArrayList();
	
		for(Appointment app : apps) {
			   SimplifiedAppointment sApp= new SimplifiedAppointment();
			   sApp.simplifyAppointment(app);
			   appLIST.add(sApp);
			  }
		
		/*for(SimplifiedAppointment sApp : appLIST) {
			   System.out.println(sApp.getDescription());
		}*/
		
		tableId.setItems(appLIST);
		tableId.getColumns().addAll(hourColumn, visitType);
		
		/*for (int i = 0; i < visitsType.size() && i<hours.size(); i++) {
		visitType.setCellValueFactory(new PropertyValueFactory<Appointment, String>(visitsType.get(i)));
		//HourColumn.setCellFactory(new PropertyValueFactory<Appointment, String>(new DateTimeStringConverter().toString(hours.get(i))));
		}*/
		
		//db.saveChangesInDatabase();
		db.close();
	}
	private void showPersonDetails(SimplifiedAppointment app) {
	    if (app != null) {
	    	nameLabel.setText(app.getAppointment().getAnimal().getName());
	        ageLabel.setText(Double.toString(app.getAppointment().getAnimal().getAge()));
	        typeLable.setText(app.getAppointment().getAnimal().getType());
	        raceLabel.setText(app.getAppointment().getAnimal().getRace());
	        phoneLable.setText(app.getAppointment().getAnimal().getOwnerPhone());
	        nameOwnerLabel.setText(app.getAppointment().getAnimal().getOwnerName());
	    } else {
	    	nameLabel.setText(" ");
	        ageLabel.setText(" ");
	        typeLable.setText(" ");
	        raceLabel.setText(" ");
	        phoneLable.setText(" ");
	        nameOwnerLabel.setText(" ");
	    }
	}
	@FXML public void handleMouseClick(MouseEvent arg0) {
	    tableId.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateTableFromDB();
		//showPersonDetails(null);

		// Listen for selection changes and show the person details when changed.
	    
	}

}
