package myClinic;

//import java.io.Serializable;
//import javax.persistence.*;
import java.sql.Time;
//import java.util.Date;
//import myClinic.Appointment;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class SimplifiedAppointment{
	private Time hour;

	private String type;
	
	@ManyToOne
	@JoinColumn(name="idappontment")
	private Appointment appointment;

	public SimplifiedAppointment() {
	}

	public Time getHour() {
		return this.hour;
	}

	public void setHour(Time hour) {
		this.hour = hour;
	}

	public Appointment getAppointment() {
		return this.appointment;
	}
	
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void simplifyAppointment(Appointment app) {
		setHour(app.getHour());
		setType(app.getType());
	}

}
