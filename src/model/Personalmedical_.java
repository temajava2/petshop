package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-11-15T02:06:53.785+0200")
@StaticMetamodel(Personalmedical.class)
public class Personalmedical_ {
	public static volatile SingularAttribute<Personalmedical, Integer> idPersonalMedical;
	public static volatile SingularAttribute<Personalmedical, String> nume;
	public static volatile SingularAttribute<Personalmedical, String> pozitieInCabinet;
	public static volatile SingularAttribute<Personalmedical, Integer> varsta;
	public static volatile ListAttribute<Personalmedical, Programari> programaris;
}
