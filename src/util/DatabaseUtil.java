package util;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import myClinic.Appointment;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
    private static String DATE_PATTERN = "dd.MM.yyyy";
    private static DateTimeFormatter DATE_FORMATTER = 
            DateTimeFormatter.ofPattern(DATE_PATTERN);

	public void setConnectionToDatabase() {
		entityManagerFactory = Persistence.createEntityManagerFactory("myClinic");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void start() {
		entityManager.getTransaction().begin();
	}
	
	public void saveChangesInDatabase() {
		entityManager.getTransaction().commit();
	}
	
	public void close() {
		entityManager.close();
	}
  
    public static String format(Date date) {
        if (date == null) {
            return null;
        }
        return DATE_FORMATTER.format((TemporalAccessor) date);
    }
    
	public List<Appointment> appointmentList() {
		List<Appointment> appointmentlList = (List<Appointment>)entityManager.createQuery("SELECT a FROM Appointment a",Appointment.class).getResultList();
		return appointmentlList;
	}
}
