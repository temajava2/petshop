package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-11-15T02:06:53.677+0200")
@StaticMetamodel(Animal.class)
public class Animal_ {
	public static volatile SingularAttribute<Animal, Integer> idAnimal;
	public static volatile SingularAttribute<Animal, Integer> animalAge;
	public static volatile SingularAttribute<Animal, String> animalName;
	public static volatile SingularAttribute<Animal, String> animalType;
	public static volatile ListAttribute<Animal, Programari> programaris;
}
