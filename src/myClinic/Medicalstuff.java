package myClinic;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medicalstuff database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalstuff.findAll", query="SELECT m FROM Medicalstuff m")
public class Medicalstuff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idmedicalStuff;

	private String name;

	private String phoneNumber;

	private String studies;

	private String type;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalstuff")
	private List<Appointment> appointments;

	public Medicalstuff() {
	}

	public int getIdmedicalStuff() {
		return this.idmedicalStuff;
	}

	public void setIdmedicalStuff(int idmedicalStuff) {
		this.idmedicalStuff = idmedicalStuff;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStudies() {
		return this.studies;
	}

	public void setStudies(String studies) {
		this.studies = studies;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalstuff(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalstuff(null);

		return appointment;
	}

}