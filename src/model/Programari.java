package model;

import java.io.Serializable;
import javax.persistence.*;
//import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the programari database table.
 * 
 */
@Entity
@NamedQuery(name="Programari.findAll", query="SELECT p FROM Programari p")
public class Programari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramari;

	@Temporal(TemporalType.DATE)
	private Date dataProgramare;

	private byte necesitaInternare;

	private String oraProgramare;

	private String tipConsult;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idPersonalMedical")
	private Personalmedical personalmedical;

	public Programari() {
	}

	public int getIdProgramari() {
		return this.idProgramari;
	}

	public void setIdProgramari(int idProgramari) {
		this.idProgramari = idProgramari;
	}

	public Date getDataProgramare() {
		return this.dataProgramare;
	}

	public void setDataProgramare(Date dataProgramare) {
		this.dataProgramare = dataProgramare;
	}

	public byte getNecesitaInternare() {
		return this.necesitaInternare;
	}

	public void setNecesitaInternare(byte necesitaInternare) {
		this.necesitaInternare = necesitaInternare;
	}

	public String getOraProgramare() {
		return this.oraProgramare;
	}

	public void setOraProgramare(String string) {
		this.oraProgramare = string;
	}

	public String getTipConsult() {
		return this.tipConsult;
	}

	public void setTipConsult(String tipConsult) {
		this.tipConsult = tipConsult;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}