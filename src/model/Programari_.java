package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-11-15T02:06:53.797+0200")
@StaticMetamodel(Programari.class)
public class Programari_ {
	public static volatile SingularAttribute<Programari, Integer> idProgramari;
	public static volatile SingularAttribute<Programari, Date> dataProgramare;
	public static volatile SingularAttribute<Programari, Byte> necesitaInternare;
	public static volatile SingularAttribute<Programari, String> oraProgramare;
	public static volatile SingularAttribute<Programari, String> tipConsult;
	public static volatile SingularAttribute<Programari, Animal> animal;
	public static volatile SingularAttribute<Programari, Personalmedical> personalmedical;
}
