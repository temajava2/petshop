package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAnimal;

	private int animalAge;

	private String animalName;

	private String animalType;

	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="animal")
	private List<Programari> programaris;

	public Animal() {
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public int getAnimalAge() {
		return this.animalAge;
	}

	public void setAnimalAge(int animalAge) {
		this.animalAge = animalAge;
	}

	public String getAnimalName() {
		return this.animalName;
	}

	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	public String getAnimalType() {
		return this.animalType;
	}

	public void setAnimalType(String animalType) {
		this.animalType = animalType;
	}

	public List<Programari> getProgramaris() {
		return this.programaris;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.programaris = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setAnimal(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setAnimal(null);

		return programari;
	}

}